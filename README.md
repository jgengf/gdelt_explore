# Objective #

This task is meant to leverage the information in GDELT dataset and detect business events related with companies we are interested in.

Up to Mar 2017, there're two versions of the key datasets in GDELT. One of the big difference from version 1 to 2 is the daily feed is updated with 15 mins' feed. The other functional change coming with version 2 is the expanding of features. More details are on the official website as described below.

## GDELT Event Database ##

The GDELT Event Database records over 300 categories of physical activities around the world, from riots and protests to peace appeals and diplomatic exchanges, georeferenced to the city or mountaintop, across the entire planet dating back to January 1, 1979 and updated every 15 minutes.

Essentially it takes a sentence like "The United States criticized Russia yesterday for deploying its troops in Crimea, in which a recent clash with its soldiers left 10 civilians injured" and transforms this blurb of unstructured text into three structured database entries, recording US CRITICIZES RUSSIA, RUSSIA TROOP-DEPLOY UKRAINE (CRIMEA), and RUSSIA MATERIAL-CONFLICT CIVILIANS (CRIMEA).

Nearly 60 attributes are captured for each event, including the approximate location of the action and those involved. This translates the textual descriptions of world events captured in the news media into codified entries in a grand "global spreadsheet."


More about this database can be found here: http://blog.gdeltproject.org/gdelt-2-0-our-global-world-in-realtime/

Code book: http://data.gdeltproject.org/documentation/GDELT-Event_Codebook-V2.0.pdf

## GDELT Global Knowledge Graph ##

Much of the true insight captured in the world's news media lies not in what it says, but the context of how it says it. The GDELT Global Knowledge Graph (GKG) compiles a list of every person, organization, company, location and several million themes and thousands of emotions from every news report, using some of the most sophisticated named entity and geocoding algorithms in existance, designed specifically for the noisy and ungrammatical world that is the world's news media.
The resulting network diagram constructs a graph over the entire world, encoding not only what's happening, but what its contextis, who's involved, and how the world is feeling about it, updated every single day.

More about this database can be found here: http://blog.gdeltproject.org/gdelt-2-0-our-global-world-in-realtime/

Code book: http://data.gdeltproject.org/documentation/GDELT-Global_Knowledge_Graph_Codebook-V2.1.pdf